FROM ubuntu:18.04
ARG JAVA_VERSION=11
ARG JAVA_RELEASE=JDK

RUN bash -c ' \
    set -euxo pipefail && \
    apt-get update && \
    pkg="openjdk-$JAVA_VERSION"; \
    if [ "$JAVA_RELEASE" = "JDK" ]; then \
        pkg="$pkg-jdk-headless"; \
    else \
        pkg="$pkg-jre-headless"; \
    fi; \
    apt-get install -y --no-install-recommends "$pkg" && \
    apt-get clean'



ENV JAVA_HOME=/usr
RUN export JAVA_HOME

RUN java --version

COPY ./ /home/src/
WORKDIR /home/src
RUN ./gradlew -Dspring.profiles.active=prod -Pprod -Pswagger build --refresh-dependencies
WORKDIR /home/src/build/libs
EXPOSE ${SERVER_PORT:-8080}
ENTRYPOINT ["java", "-jar", "app.jar", "-Dserver.port=${SERVER_PORT:-8080}"]

