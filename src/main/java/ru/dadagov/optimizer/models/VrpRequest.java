package ru.dadagov.optimizer.models;


import javax.validation.constraints.NotNull;

public class VrpRequest {
    @NotNull
    public Long[][] distanceMatrix;

    public Integer depot;

    public Integer vehicleNumber;

    public Integer[][] pickupsDeliveries;
}
