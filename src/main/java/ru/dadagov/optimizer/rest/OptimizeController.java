package ru.dadagov.optimizer.rest;


import com.google.ortools.Loader;
import com.google.ortools.constraintsolver.*;
import com.google.protobuf.Duration;
import com.skaggsm.ortools.OrToolsHelper;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.dadagov.optimizer.models.VrpRequest;

import java.util.ArrayList;
import java.util.logging.Logger;


@RestController
@RequestMapping("/api")
public class OptimizeController {

    private static final Logger logger = Logger.getLogger(OptimizeController.class.getName());

    static {
        OrToolsHelper.loadLibrary();
    }

    @PostMapping("/vrp")
    public Integer[] vrp(@RequestBody() VrpRequest data) {
        if (data.vehicleNumber == null) {
            data.vehicleNumber = 1;
        }
        if (data.depot == null) {
            data.depot = 0;
        }
        if (data.pickupsDeliveries == null) {
            data.pickupsDeliveries = new Integer[0][];
        }

        RoutingIndexManager manager =

                new RoutingIndexManager(data.distanceMatrix.length, data.vehicleNumber, data.depot);

        RoutingModel routing = new RoutingModel(manager);

        final int transitCallbackIndex =
                routing.registerTransitCallback((long fromIndex, long toIndex) -> {
                    // Convert from routing variable Index to user NodeIndex.
                    int fromNode = manager.indexToNode(fromIndex);
                    int toNode = manager.indexToNode(toIndex);
                    return data.distanceMatrix[fromNode][toNode];
                });

        routing.setArcCostEvaluatorOfAllVehicles(transitCallbackIndex);

        routing.addDimension(transitCallbackIndex, // transit callback index
                0, // no slack
                Long.MAX_VALUE, // vehicle maximum travel distance
                true, // start cumul to zero
                "Distance");
        RoutingDimension distanceDimension = routing.getMutableDimension("Distance");
        distanceDimension.setGlobalSpanCostCoefficient(100);

        Solver solver = routing.solver();


        for (Integer[] request : data.pickupsDeliveries) {

            long pickupIndex = manager.nodeToIndex(request[0]);

            long deliveryIndex = manager.nodeToIndex(request[1]);

            routing.addPickupAndDelivery(pickupIndex, deliveryIndex);


            solver.addConstraint(
                    solver.makeEquality(routing.vehicleVar(pickupIndex), routing.vehicleVar(deliveryIndex)));
            solver.addConstraint(solver.makeLessOrEqual(
                    distanceDimension.cumulVar(pickupIndex), distanceDimension.cumulVar(deliveryIndex)));

        }

        RoutingSearchParameters searchParameters =
                main.defaultRoutingSearchParameters()
                        .toBuilder()
                        .setTimeLimit(Duration.newBuilder().setSeconds(60).build())
                        .setSolutionLimit(1000)
                        .setFirstSolutionStrategy(FirstSolutionStrategy.Value.PATH_CHEAPEST_ARC)
                        .build();

        Assignment solution = routing.solveWithParameters(searchParameters);

        try {
            return getVrpResult(data, routing, manager, solution);
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }


    private Integer[] getVrpResult(
            VrpRequest data, RoutingModel routing, RoutingIndexManager manager, Assignment solution) {
        long totalDistance = 0;
        ArrayList<Integer> ordered = new ArrayList<Integer>();
        for (int i = 0; i < data.vehicleNumber; ++i) {
            long index = routing.start(i);
            logger.info("Route for Vehicle " + i + ":");
            long routeDistance = 0;
            String route = "";
            while (!routing.isEnd(index)) {
                int point = manager.indexToNode(index);
                ordered.add(point);
                route += point;
                long previousIndex = index;
                index = solution.value(routing.nextVar(index));
                if (!routing.isEnd(index)) {
                    route += " -> ";
                    routeDistance += routing.getArcCostForVehicle(previousIndex, index, i);
                    logger.info("Point: " + point + " distance: " + routeDistance + " Next Point: " + index);
                }

            }
            logger.info(route);
            logger.info("Distance of the route: " + routeDistance + "m");
            totalDistance += routeDistance;
        }
        logger.info("Total Distance of all routes: " + totalDistance + "m");
        return ordered.toArray(new Integer[0]);
    }

}
